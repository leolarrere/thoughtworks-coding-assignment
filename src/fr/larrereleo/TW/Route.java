package fr.larrereleo.TW;

/**
 * <b>The class Route represents a route between two towns.</b>
 * <p>
 * A route is represented by :
 * <ul>
 * <li>A town of origin.</li>
 * <li>A town of destination.</li>
 * <li>The distance between these two towns.</li>
 * </ul>
 * </p>
 * 
 * @author Leo Larrere
 * @version 1.0
 */
public class Route {

	/**
	 * <p>Town of origin.</p>
	 */
	private String origin;

	/**
	 * <p>Town of destination.</p>
	 */	
	private String destination;

	/**
	 * <p>Distance between origin and destination.</p>
	 */
	private int distance;

	/**
	 * <p>Route constructor with 3 parameters :
	 * <ul>
	 * <li>A town of origin.</li>
	 * <li>A town of destination.</li>
	 * <li>The distance between these two towns.</li>
	 * </ul>
	 * </p>
	 *
	 * @see Route#origin
	 * @see Route#destination
	 * @see Route#distance
	 */
	public Route(String origin, String destination, int distance)
	{
		this.origin = origin;
		this.destination = destination;
		this.distance = distance;
	}

	/**
	 * <p>Compares the route's town of origin with the given parameter.</p>
	 *
	 * @return true if the towns are the same, false if not
	 *
	 * @see Route#origin
	 */
	public boolean compareOrigin(String origin)
	{
		if (this.origin.equals(origin))
			return true;
		return false;
	}

	/**
	 * <p>Compares the route's town of destination with the given parameter.</p>
	 *
	 * @return true if the towns are the same, false if not
	 *
	 * @see Route#destination
	 */	
	public boolean compareDestination(String destination)
	{
		if (this.destination.equals(destination))
			return true;
		return false;
	}

	/**
	 * <p>Returns the route's town of origin.</p>
	 *
	 * @return the route's town of origin
	 *
	 * @see Route#origin
	 */
	public String getOrigin()
	{
		return this.origin;
	}

	/**
	 * <p>Returns the route's town of destination.</p>
	 *
	 * @return the route's town of destination
	 *
	 * @see Route#destination
	 */
	public String getDestination()
	{
		return this.destination;
	}

	/**
	 * <p>Returns the route's distance.</p>
	 *
	 * @return the route's distance
	 *
	 * @see Route#distance
	 */	
	public int getDistance()
	{
		return this.distance;
	}	

}
