package fr.larrereleo.TW;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ListIterator;


/**
 * <b>The class Graph represents Kiwiland's railroad map.</b>
 * <p>
 * A graph is represented by :
 * <ul>
 * <li>A list of Route instances.</li>
 * <li>A unique input file.</li>
 * </ul>
 * </p>
 * 
 * @see Route
 * 
 * @author Leo Larrere
 * @version 1.0
 */
public class Graph {

	/**
	 * <p>The different routes of Kiwiland.</p>
	 * @see Route
	 */
	private LinkedList<Route> graph;

	/**
	 * <p>File from which the graph is loaded.</p>
	 * <p>It cannot be modified by the application.</p>
	 */
	private final String FILE_NAME = "input.txt";	

	/**
	 * <p>Default Graph constructor.</p>
	 * <p>When a Graph instance is created, the constructor initializes its route list to an empty list.</p>
	 * @see Graph#graph
	 */
	public Graph() 
	{
		this.graph = new LinkedList<Route>();
	}

	/**
	 * <p>Reads the input file and saves its content in the Route list.</p>
	 * 
	 * @throws IOException
	 * 
	 * @see Graph#FILE_NAME
	 * @see Graph#graph
	 */
	public void loadGraph()
	{								
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));

			//Only reads the first line, since the input only has one line.
			String fileContent = br.readLine();

			//Stores all routes in an array of String
			String[] routes = fileContent.split(", ");

			//For each route
			for (int i = 0; i < routes.length ; i++)
			{
				//Fetch the route's origin
				String origin = routes[i].substring(0,1);
				//It's destination
				String destination = routes[i].substring(1,2);
				//And the distance between them
				int distance = Integer.parseInt(routes[i].substring(2,3));

				//New Route instance
				Route r = new Route(origin,destination,distance);

				//Adds the route to the graph
				this.graph.add(r);		
			}
			br.close();
		} 
		catch (IOException e) 
		{	
			//In case an IOException occurs
			System.err.println("Input file could not be loaded. Application has terminated.");
			System.exit(0);
		}

	}

	/**
	 * <p>Calculates the total distance of a route.</p>
	 * @param route 
	 * String containing the route (e.g. A-B-D-E).
	 * @return
	 * Total distance of the route.
	 */	
	public String calculateDistance(String route)
	{
		//Stores all towns in an array of String
		String[] towns = route.split("-");

		//Total distance of the route
		int totalDistance = 0;

		//For all towns
		for (int i = 0; i < towns.length-1; i++)
		{
			//Tries to get a distance between the current town and the next one
			int distance = this.getDistance(towns[i],towns[i+1]);

			//In there exists no route between these 2 towns
			if (distance==0)
				return "NO SUCH ROUTE";

			//If there is a route, adds the distance between the 2 towns to the total distance
			totalDistance += distance;
		}
		//Lazy way to transform an int into a String
		return totalDistance + "";		
	}

	/**
	 * <p>Finds the number of trips between two towns within a specific amount of stops.</p>
	 * <p>The trip can be a loop.</p>
	 * @param origin 
	 * String containing the town of origin.
	 * @param destination 
	 * String containing the town of destination.
	 * @param minStops 
	 * Minimum number of stops.
	 * @param maxStops 
	 * Maximum number of stops.
	 * @param iteration 
	 * The number of stops done so far.
	 * @return
	 * Number of trips found between the two towns.
	 */	
	public int findTripsByIteration(String origin, String destination, int minStops, int maxStops, int iteration)
	{
		//To iterate over the list of routes
		ListIterator<Route> li = this.graph.listIterator();

		//Number of trips found
		int num = 0;

		//While the list is not empty
		while (li.hasNext())
		{
			//Store the element in a new Route instance
			Route r = li.next();

			//If the route starts from the current location
			//And if the maximum number of stops has not been reached yet
			if (r.compareOrigin(origin) && iteration+1 < maxStops)
				//Recursive call on this route
				num += findTripsByIteration(r.getDestination(), destination, minStops, maxStops, iteration+1);		

			//If the route starts from the current location and ends at the final destination
			if (r.compareOrigin(origin) && r.compareDestination(destination))
				//If the number of stops is not too low or too high
				if (iteration+1 >= minStops && iteration+1 <= maxStops)
					//Then a trip has been found
					num++;		
		}
		//Returns the number of trips found
		return num;
	}

	/**
	 * <p>Finds the number of trips between two towns within a specific distance.</p>
	 * <p>The trip can be a loop.</p>
	 * @param origin 
	 * String containing the town of origin.
	 * @param destination 
	 * String containing the town of destination.
	 * @param distance 
	 * Distance traveled so far.
	 * @param maxDistance 
	 * Maximum distance of travel.
	 * @return
	 * Number of trips found between the two towns.
	 */		
	public int findTripsByDistance(String origin, String destination, int distance, int maxDistance)
	{
		//To iterate over the list of routes
		ListIterator<Route> li = this.graph.listIterator();

		//Number of trips found
		int num = 0;

		//While the list is not empty
		while (li.hasNext())
		{
			//Store the element in a new Route instance
			Route r = li.next();

			//If the route starts from the current location
			//And if the total distance from the current location to the route's destination is not too high
			if (r.compareOrigin(origin) && (distance + r.getDistance()) < maxDistance)
				//Recursive call on this route
				num += findTripsByDistance(r.getDestination(), destination, distance + r.getDistance(), maxDistance);		

			//If the route starts from the current location and ends at the final destination
			if (r.compareOrigin(origin) && r.compareDestination(destination))
				//If the total distance from the current location to the route's destination is not too high
				if (distance + r.getDistance() < maxDistance)
					//Then a trip has been found
					num++;		
		}
		//Return the number of trips found
		return num;
	}	

	/**
	 * <p>Finds the shortest route between 2 towns.</p>
	 * @param origin 
	 * String containing the town of origin.
	 * @param destination 
	 * String containing the town of destination.
	 * @param distance 
	 * Distance traveled so far.
	 * @return
	 * Shortest route between the twos towns.
	 */	
	public int shortestRoute(String origin, String destination, int distance) 
	{
		//To iterate over the list of routes
		ListIterator<Route> li = this.graph.listIterator();

		//Variables used to calculate the shortest distance
		int d1, d2, minLength;
		//Since there is no infinite in Java, they are set to a high number
		d1 = d2 = minLength = 999999;

		//While the list is not empty
		while (li.hasNext())
		{
			//Store the element in a new Route instance
			Route r = li.next();

			//If the route starts from the current location
			//And if the distance done so far is not equivalent to the total length of the railroad system
			if (r.compareOrigin(origin) && distance < graphTotalLength())
				//Recursive call on this route
				d1 = shortestRoute(r.getDestination(), destination, distance + r.getDistance());		

			//If the route starts from the current location and ends at the final destination
			if (r.compareOrigin(origin) && r.compareDestination(destination))
				//Then a trip has been found, and the total distance traveled is calculated
				d2 = distance + r.getDistance();	

			//Founds out the minimum between the two distances and the shortest route found so far
			minLength = Math.min(Math.min(d1, d2), minLength);
		}
		//And returns it
		return minLength;
	}

	/**
	 * <p>Finds out if there is a route between two towns.</p>
	 * <p>If the route exists, its length of the route is returned.</p>
	 * <p>If there is no such route, 0 is returned.</p>
	 * @param origin 
	 * String containing the town of origin.
	 * @param destination 
	 * String containing the town of destination.
	 * @return
	 * Distance between the two towns.
	 */		
	private int getDistance(String origin, String destination)
	{
		//To iterate over the list of routes
		ListIterator<Route> li = this.graph.listIterator();

		//While the list is not empty
		while (li.hasNext())
		{
			//Store the element in a new Route instance
			Route r = li.next();
			
			//If the route matches the one we are looking for
			if (r.compareOrigin(origin) && r.compareDestination(destination))
				//Return its distance
				return r.getDistance();
		}
		//In case the route was not found
		return 0;
	}

	/**
	 * <p>Calculates the whole length of the railroad system.</p>
	 * @return
	 * Total graph length
	 */	
	private int graphTotalLength()
	{
		//Total graph length
		int length = 0;
		
		//To iterate over the list of routes
		ListIterator<Route> li = this.graph.listIterator();

		//While the list is not empty
		while (li.hasNext())
		{
			//Store the element in a new Route instance
			Route r = li.next();
			//Adds the route's distance to the total length of the graph
			length += getDistance(r.getOrigin(), r.getDestination());		
		}
		//Returns total length
		return length;
	}	

	/**
	 * <p>Finds out if the graph is empty or not.</p>
	 * @return
	 * true if the graph is empty, false if not.
	 */		
	public boolean isEmpty()
	{
		if (this.graph.size()==0)
			return true;
		return false;
	}

}