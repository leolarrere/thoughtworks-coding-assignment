package fr.larrereleo.TW;

/**
 * <b>The class TrainManager handles the output of the application.</b>
 * 
 * @see Graph
 * 
 * @author Leo Larrere
 * @version 1.0
 */
public class TrainManager {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		//Creates a new Graph instance
		Graph g = new Graph();
		//Loads the input file
		g.loadGraph();
		
		//The distance of the route A-B-C
		System.out.println("Output #1 : " + g.calculateDistance("A-B-C"));
		//The distance of the route A-D
		System.out.println("Output #2 : " + g.calculateDistance("A-D"));
		//The distance of the route A-D-C
		System.out.println("Output #3 : " + g.calculateDistance("A-D-C"));
		//The distance of the route A-E-B-C-D
		System.out.println("Output #4 : " + g.calculateDistance("A-E-B-C-D"));
		//The distance of the route A-E-D
		System.out.println("Output #5 : " + g.calculateDistance("A-E-D"));
		
		//The number of trips starting at C and ending at C with a maximum of 3 stops
		System.out.println("Output #6 : " + g.findTripsByIteration("C", "C", 1, 3, 0));
		//The number of trips starting at A and ending at C with exactly 4 stops
		System.out.println("Output #7 : " + g.findTripsByIteration("A", "C", 4, 4, 0));	
		
		//The length of the shortest route (in terms of distance to travel) from A to C
		System.out.println("Output #8 : " + g.shortestRoute("A", "C", 0));
		//The length of the shortest route (in terms of distance to travel) from B to B
		System.out.println("Output #9 : " + g.shortestRoute("B", "B", 0));
		
		//The number of different routes from C to C with a distance of less than 30
		System.out.println("Output #10 : " + g.findTripsByDistance("C", "C", 0, 30));
	}
}
