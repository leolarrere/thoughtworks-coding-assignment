package fr.larrereleo.TW;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//Runs all tests.
@RunWith(Suite.class)
@SuiteClasses({ GraphTest.class, RouteTest.class })
public class AllTests {

}
