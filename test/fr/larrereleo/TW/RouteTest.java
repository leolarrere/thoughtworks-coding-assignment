package fr.larrereleo.TW;

import static org.junit.Assert.*;

import org.junit.Test;

public class RouteTest {

	@Test
	//Tests the constructor
	//Also tests the 3 getters
	public void testRoute()
	{
		Route r = new Route("A","B",10);
		assertEquals("Testing Route constructor #1", "A", r.getOrigin());
		assertEquals("Testing Route constructor #2", "B", r.getDestination());
		assertEquals("Testing Route constructor #3", 10, r.getDistance());
	}

	@Test
	//Tests if compareOrigin() works correctly
	public void testCompareOrigin() {
		Route r = new Route("A","B",10);
		assertEquals("Testing compareOrigin()", true, r.compareOrigin("A"));
	}

	@Test
	//Tests if compareDestination() works correctly
	public void testCompareDestination() {
		Route r = new Route("A","B",10);
		assertEquals("Testing compareDestination()", true, r.compareDestination("B"));
	}
}
