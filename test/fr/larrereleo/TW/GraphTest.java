package fr.larrereleo.TW;

import static org.junit.Assert.*;

import org.junit.Test;

//JUnit tests on Graph class
public class GraphTest 
{
	@Test
	//Tests the constructor
	//The route list should be empty when a Graph instance is created
	public void testGraph() 
	{
		Graph g = new Graph();
		assertTrue("The graph should be empty.",g.isEmpty());
	}

	@Test
	//Tests if loading a graph works correctly
	//Does not test the correctness of the graph, only if it is not empty anymore
	public void testLoadGraph() 
	{
		Graph g = new Graph();
		g.loadGraph();
		assertFalse("The graph should be not be empty.",g.isEmpty());
	}

	@Test
	//Tests if the distance calculated is correct
	public void testCalculateDistance() 
	{
		Graph g = new Graph();
		g.loadGraph();

		//Testing output #1
		String returnValue = g.calculateDistance("A-B-C");		
		assertEquals("Testing output #1","9",returnValue);

		//Testing output #2
		returnValue = g.calculateDistance("A-D");		
		assertEquals("Testing output #2","5",returnValue);
		
		//Testing output #3
		returnValue = g.calculateDistance("A-D-C");		
		assertEquals("Testing output #3","13",returnValue);
		
		//Testing output #4
		returnValue = g.calculateDistance("A-E-B-C-D");		
		assertEquals("Testing output #4","22",returnValue);
		
		//Testing output #5
		returnValue = g.calculateDistance("A-E-D");		
		assertEquals("Testing output #5","NO SUCH ROUTE",returnValue);
	}
	
	@Test
	//Tests if the number of trips is correct (by iteration)
	public void testFindTripsByIteration() 
	{
		Graph g = new Graph();
		g.loadGraph();
		
		//Testing output #6
		assertEquals("Testing output #6",2,g.findTripsByIteration("C", "C", 1, 3, 0));
		
		//Testing output #7
		assertEquals("Testing output #7",3,g.findTripsByIteration("A", "C", 4, 4, 0));
		
		//Some more tests on findTripByIteration() just to be sure
		assertEquals("Testing findTripsByIteration() #1",1,g.findTripsByIteration("B", "B", 1, 3, 0));		
		assertEquals("Testing findTripsByIteration() #2",4,g.findTripsByIteration("A", "E", 5, 5, 0));
		assertEquals("Testing findTripsByIteration() #3",10,g.findTripsByIteration("C", "C", 1, 6, 0));
	}
	
	@Test
	//Test if the shortest route was indeed found
	public void TestShortestRoute() 
	{
		Graph g = new Graph();
		g.loadGraph();
		
		//Testing output #8
		assertEquals("Testing output #8",9,g.shortestRoute("A", "C", 0));
		
		//Testing output #9
		assertEquals("Testing output #8",9,g.shortestRoute("A", "C",0));
		
		//Some more tests on shortestRoute() just to be sure
		assertEquals("Testing shortestRoute() #1",5,g.shortestRoute("A", "D",0));
		assertEquals("Testing shortestRoute() #2",12,g.shortestRoute("B", "D",0));
		assertEquals("Testing shortestRoute() #3",999999,g.shortestRoute("B", "A",0));
	}	
	
	@Test
	//Tests if the number of trips is correct (by distance)
	public void testFindTripsByDistance() 
	{
		Graph g = new Graph();
		g.loadGraph();
		
		//Testing output #10
		assertEquals("Testing output #10",7,g.findTripsByDistance("C", "C", 0, 30));
		
		//Some more tests on findTripByDistance() just to be sure
		assertEquals("Testing findTripsByIteration() #2",6,g.findTripsByDistance("A", "D", 0, 30));
		assertEquals("Testing findTripsByIteration() #3",2,g.findTripsByDistance("A", "D", 0, 20));
	}	
	
}
